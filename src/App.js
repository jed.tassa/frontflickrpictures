import React from 'react';

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      search: '',
      data: [],
    };
  }
  componentDidMount(){
   
  }
  updateSearch = (event) => {
    console.log(event.target.value)
    this.setState({
      search: event.target.value,
    });
  }


  submit = () => {
    fetch(`http://localhost:8080/${this.state.search}`).then(response => response.json())
    .then(data => this.setState({data: data.result}));
  }

  render() {
    console.log(this.state.data);
    return (
      <div>
        <input
          type="search"
          onChange={this.updateSearch}
          value={this.state.search}
        />
        <button onClick={this.submit}>Submit</button>
       
      <div>
      {this.state.data.map(data => {
          return (
             <div>
                <img src={data.url} />
             </div>
          );
      })}
   </div>
   </div>
    ); 
  }
}